## Overview

This learning to read application is build using Vue 3 (with the Composition API).

## Description

This application shows sentences:

![Step 1](/src/assets/screenshot_step1.png?raw=true "Screenshot - Step 1")

Each word should be clicked on and the word is read aloud:

![Step 2](/src/assets/screenshot_step2.png?raw=true "Screenshot - Step 2")

Once each word in the sentence has been clicked, the full sentence will be read aloud and a picture is shown:

![Step 3](/src/assets/screenshot_step3.png?raw=true "Screenshot - Step 3")

## Website

[]()

## Technology

This application is build using Vue 3 (with the Composition API).
