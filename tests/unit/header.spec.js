import { shallowMount } from '@vue/test-utils'
import Header from '@/components/Header.vue'

describe('Header.vue Test', () => {
  it('renders message when component is created', () => {
    // render the component
    const wrapper = shallowMount(Header, {
      props: {
        title: 'Reading App',
        index: 2,
        total: 5
      }
    })

    // check that two h2 elements are displayed
    const headings = wrapper.findAll('h2')
    expect(headings.length).toEqual(2)
    expect(headings[0].text()).toMatch('Reading App')
    expect(headings[1].text()).toMatch('3 / 5')
  })
})
